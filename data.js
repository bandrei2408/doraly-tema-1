(function() {

  var input = "A1,B1,C1\nA1,B1,C2\nA1,B1,C3\nA1,B2,C4\nA1,B2,C5\nA1,B3,C6\nA2,B4,C7\nA2,B5,C8\nA2,B5,C9\nA3,B6,C10";

  window.getData = function(){
    var tree = new Node("root", 0);
    var matrix = [];
    var lines = input.split('\n');
    for (var i in lines) {
      var line = lines[i];
      var parsedObject = {};
      var values = line.split(',');
      tree.addChild(new Node(values[0], 1)).addChild(new Node(values[1], 2)).addChild(new Node(values[2], 3));
      matrix.push([values[0].substr(1), values[1].substr(1), values[2].substr(1)]);
    }
    return {
      "treeRepresentation": tree,
      "matrixRepresentation": matrix
    };
  };

})();
