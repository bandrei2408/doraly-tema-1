(function() {

  var TABLE_EL_ID = "table";
  var HIDDEN_CLASS = "hidden";
  var TABLE_ENTRY_CLASS = "entry";
  var ALL_VALUE = "all";

  var HASH = {
    "A": 0,
    "B": 1,
    "C": 2
  }

  window.Table = {

    data: {},

    $table: null,

    $selectElements: {
      "A": null,
      "B": null,
      "C": null
    },

    init: function(data) {
      this.$table = document.getElementById(TABLE_EL_ID);
      this.$selectElements.A = document.getElementById("A");
      this.$selectElements.B = document.getElementById("B");
      this.$selectElements.C = document.getElementById("C");
      this.data.matrix = data.matrixRepresentation;
      this.data.rootNode = data.treeRepresentation;
      this.populateHtml();
      this.setListeners();
    },

    populateHtml: function() {
      var dataMatrixRepresentation = this.data.matrix;
      for (var i in dataMatrixRepresentation) {
        this.createOptionsForEntry(dataMatrixRepresentation[i]);
        this.createTableEntry(dataMatrixRepresentation[i][0], dataMatrixRepresentation[i][1], dataMatrixRepresentation[i][2])
      }
    },

    createOptionsForEntry: function(entry) {
      this.createOption("A", entry[0]);
      this.createOption("B", entry[1]);
      this.createOption("C", entry[2]);
    },

    createOption: function(id, value) {
      var $selectEl = document.getElementById(id);
      if (!$selectEl.options[value]) {
        var $option = document.createElement('option');
        $option.text = id + value;
        $option.value = value;
        $selectEl.add($option);
      }
    },

    createTableEntry: function(a, b, c) {
      var $entry = document.createElement('div');
      $entry.className = TABLE_ENTRY_CLASS;
      $entry.innerHTML = "A" + a + ",B" + b + ",C" + c;
      $entry.dataset.a = a;
      $entry.dataset.b = b;
      $entry.dataset.c = c;
      this.$table.appendChild($entry);
    },

    setListeners: function() {
      this.$selectElements.A.addEventListener("change", this.hChangeSelect.bind(this));
      this.$selectElements.B.addEventListener("change", this.hChangeSelect.bind(this));
      this.$selectElements.C.addEventListener("change", this.hChangeSelect.bind(this));
    },

    getIdFromHash: function(hashValue) {
      for (var key in HASH) {
        if(HASH[key] == hashValue) {
          return key;
        }
      }
      return null;
    },

    hChangeSelect: function(e) {
      var id = e.target.id;
      var changedValue = e.target.value;
      var nodeValue = id + changedValue;
      var prevHashValue = HASH[id] - 1;
      if(changedValue === ALL_VALUE && prevHashValue < 0) {
        nodeValue = "root";
      } else if (changedValue === ALL_VALUE && prevHashValue >= 0){
        var prevId = this.getIdFromHash(prevHashValue);
        nodeValue = prevId + this.$selectElements[prevId].value;
      }
      var selectedNode = this.data.rootNode.findNode(nodeValue);
      this.resetSelects();
      if(changedValue !== ALL_VALUE) {
        this.setPreviousSelectValues(selectedNode);
        this.setCurrentSelectValues(selectedNode);
      }
      this.setNextSelectValuesHiddenAndAll(selectedNode);
      this.setNextSelectValues(selectedNode);
      this.checkLastSelect();
      this.setEntries();
    },

    resetSelects: function() {
      var $options = document.getElementsByTagName('option');
      for (var i = 0; i < $options.length; i++) {
        $options[i].classList.remove(HIDDEN_CLASS);
      }
    },

    setPreviousSelectValues: function(node) {
      var previousNode = node.parent;
      if (previousNode === this.data.rootNode) {
        return;
      }
      var id = previousNode.value[0];
      var value = previousNode.value.substr(1);
      var $selectElement = this.$selectElements[id];
      $selectElement.value = value;
      for (var i = 0; i < $selectElement.options.length; i++) {
        if($selectElement.options[i].value !== value && $selectElement.options[i].value !== ALL_VALUE) {
          $selectElement.options[i].classList.add(HIDDEN_CLASS);
        }
      }
      this.setPreviousSelectValues(previousNode);
    },

    setCurrentSelectValues: function(node) {
      var parentNode = node.parent;
      var id = node.value[0];
      var availableNodes = parentNode.children;
      var availableNodeValues = [];
      for (var i = 0; i < availableNodes.length; i++) {
        availableNodeValues.push(availableNodes[i].value.substr(1));
      }
      var $selectElement = this.$selectElements[id];
      for (var i = 0; i < $selectElement.options.length; i++) {
        if(!availableNodeValues.includes($selectElement.options[i].value) && $selectElement.options[i].value !== ALL_VALUE) {
          $selectElement.options[i].classList.add(HIDDEN_CLASS);
        }
      }
    },

    setNextSelectValuesHiddenAndAll: function(node) {
      var firstChild = node.children[0];
      if (!firstChild) {
        return;
      }
      var id = firstChild.value[0];
      var $selectElement = this.$selectElements[id];
      for (var i = 0; i < $selectElement.options.length; i++) {
        $selectElement.options[i].classList.add(HIDDEN_CLASS);
      }
      $selectElement.value = ALL_VALUE;
      this.setNextSelectValuesHiddenAndAll(firstChild);
    },

    setNextSelectValues: function(node) {
      var childrenNodes = node.children;
      if (!childrenNodes) {
        return;
      }
      for (var i in childrenNodes) {
        var childrenNode = childrenNodes[i];
        var id = childrenNode.value[0];
        var value = childrenNode.value.substr(1);
        var $selectElement = this.$selectElements[id];
        for (var i = 0; i < $selectElement.options.length; i++) {
          if($selectElement.options[i].value === value || $selectElement.options[i].value === ALL_VALUE) {
            $selectElement.options[i].classList.remove(HIDDEN_CLASS);
          }
        }
        this.setNextSelectValues(childrenNode);
      }
    },

    checkLastSelect: function() {
      var $selectElement = this.$selectElements.C;
      var noOptions = 0;
      var value = '';
      for (var i = 0; i < $selectElement.options.length; i++) {
        if($selectElement.options[i].value !== ALL_VALUE && !$selectElement.options[i].classList.contains(HIDDEN_CLASS)) {
          noOptions++;
          value = $selectElement.options[i].value;
        }
      }
      if(noOptions == 1) {
        $selectElement.options[0].classList.add(HIDDEN_CLASS);
        $selectElement.value = value;
        this.setPreviousSelectValues(this.data.rootNode.findNode("C" + value));
      }
    },

    setEntries: function() {
      this.setAllEntriesVisible();
      var query = "";
      for (var key in this.$selectElements) {
        if (this.$selectElements[key].value === ALL_VALUE){
          continue;
        }
        query += "." + TABLE_ENTRY_CLASS + ":not([data-" + key + "='" + this.$selectElements[key].value  + "']), ";
      }
      if (!query) {
        return;
      }
      query = query.slice(0, -2);
      var hiddenEntries = document.querySelectorAll(query);
      for (var i = 0; i < hiddenEntries.length; i++) {
        hiddenEntries[i].classList.add(HIDDEN_CLASS);
      }
    },

    setAllEntriesVisible: function() {
      var entries = document.getElementsByClassName(TABLE_ENTRY_CLASS);
      for (var i = 0; i < entries.length; i++) {
        entries[i].classList.remove(HIDDEN_CLASS);
      }
    },
  };

})();
