(function() {

  window.Node = function(value, level) {

    this.value = value;
    this.level = level;
    this.children = [];
    this.parent = null;

    this.addChild = function(node) {
      for (var i = 0; i < this.children.length; i++) {
        if (this.children[i].value == node.value) {
          return this.children[i];
        }
      }
      node.parent = this;
      this.children[this.children.length] = node;
      return this.children[this.children.length - 1];
    }

    this.findNode = function(value) {
      correctNode = null;
      if(this.value == value) {
        correctNode = this;
      } else {
        for (var i in this.children) {
          correctNode = this.children[i].findNode(value);
          if(correctNode) {
            break;
          }
        }
      }
      return correctNode;
    }

  }

})();
